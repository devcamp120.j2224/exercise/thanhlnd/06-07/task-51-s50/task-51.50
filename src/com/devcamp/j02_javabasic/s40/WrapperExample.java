package com.devcamp.j02_javabasic.s40;

public class WrapperExample {
    
    public static void autoBoxing() {
        // byte bte = 10;
        // short sh = 20;
        // int it = 30;
        // long lng = 40;
        // float fat = 50.0F;
        // double dbl = 60.0D;
        // char ch = 'a';
        // boolean bool = true;
        // // lần 1
        // byte bte = 11;
        // short sh = 21;
        // int it = 31;
        // long lng = 41;
        // float fat = 51.0F;
        // double dbl = 61.0D;
        // char ch = 'b';
        // boolean bool = false;

        // // lần 2
        // byte bte = 12;
        // short sh = 22;
        // int it = 32;
        // long lng = 42;
        // float fat = 52.0F;
        // double dbl = 62.0D;
        // char ch = 'c';
        // boolean bool = true;

        // // lần 3
        // byte bte = 13;
        // short sh = 23;
        // int it = 33;
        // long lng = 43;
        // float fat = 53.0F;
        // double dbl = 63.0D;
        // char ch = 'd';
        // boolean bool = false;

        // // lần 4
        // byte bte = 14;
        // short sh = 24;
        // int it = 34;
        // long lng = 44;
        // float fat = 54.0F;
        // double dbl = 64.0D;
        // char ch = 'e';
        // boolean bool = true;

        // lần 5
        byte bte = 16;
        short sh = 26;
        int it = 36;
        long lng = 46;
        float fat = 56.0F;
        double dbl = 66.0D;
        char ch = 'f';
        boolean bool = false;

        /***
         *  //Autoboxing: Converting primitives into objects
         * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ nguyên thuỷ sang object
         * của wrapper class tương ứng
         */

         Byte byteobj = bte;
         Short shortobj = sh;
         Integer intobj = it;
         Long longobj = lng;
         Float floatobj = fat;
         Double doubleobj = dbl;
         Character charobj = ch;
         Boolean boolobj = bool;

         System.out.println("Run 5 :");
         System.out.println("---Printing object values (In gia tri cua object)---");
         System.out.println("Byte object: " + byteobj);
         System.out.println("Short object: " + shortobj);
         System.out.println("Integer object: " + intobj);
         System.out.println("Long object: " + longobj);
         System.out.println("Float object: " + floatobj);
         System.out.println("Double object: " + doubleobj);
         System.out.println("Character object: " + charobj);
         System.out.println("Boolean object: " + boolobj);   

    }

    public static void unBoxing(){
        // byte bte = 10;
        // short sh = 20;
        // int it = 30;
        // long lng = 40;
        // float fat = 50.0F;
        // double dbl = 60.0D;
        // char ch = 'a';
        // boolean bool = true;

        // lần 1
        // byte bte = 11;
        // short sh = 21;
        // int it = 31;
        // long lng = 41;
        // float fat = 51.0F;
        // double dbl = 61.0D;
        // char ch = 'b';
        // boolean bool = false;

        // // lần 2
        // byte bte = 12;
        // short sh = 22;
        // int it = 32;
        // long lng = 42;
        // float fat = 52.0F;
        // double dbl = 62.0D;
        // char ch = 'c';
        // boolean bool = true;

        // // lần 3
        // byte bte = 13;
        // short sh = 23;
        // int it = 33;
        // long lng = 43;
        // float fat = 53.0F;
        // double dbl = 63.0D;
        // char ch = 'd';
        // boolean bool = false;

        // // lần 4
        // byte bte = 14;
        // short sh = 24;
        // int it = 34;
        // long lng = 44;
        // float fat = 54.0F;
        // double dbl = 64.0D;
        // char ch = 'e';
        // boolean bool = true;

        // lần 5
        byte bte = 16;
        short sh = 26;
        int it = 36;
        long lng = 46;
        float fat = 56.0F;
        double dbl = 66.0D;
        char ch = 'f';
        boolean bool = false;

        
        //Autoboxing: Converting primitives into objects
         Byte byteobj = bte;
         Short shortobj = sh;
         Integer intobj = it;
         Long longobj = lng;
         Float floatobj = fat;
         Double doubleobj = dbl;
         Character charobj = ch;
         Boolean boolobj = bool;
         /***
          * Unboxing: Converting Objects to Primitives
          *  Unboxing là cơ chế tự động chuyển đổi các object
          * của wrapper class sang kiểu dữ liệu nguyên thuỷ tương ứng
          */
          Byte bytevalue = byteobj;
          Short shortvalue = shortobj;
          Integer intvalue = intobj;
          Long longvalue = longobj;
          Float floatvalue = floatobj;
          Double doublevalue = doubleobj;
          Character charvalue = charobj;
          Boolean boolvalue = boolobj;

          System.out.println("Run 5 :");
          System.out.println("---Printing primitive values (In gia tri cua primitive data types (Kieu du lieu nguyen thuy)---");
          System.out.println("Byte value: " + bytevalue);
          System.out.println("Short value: " + shortvalue);
          System.out.println("Integer value: " + intvalue);
          System.out.println("Long value: " + longvalue);
          System.out.println("Float value: " + floatvalue);
          System.out.println("Double value: " + doublevalue);
          System.out.println("Character value: " + charvalue);
          System.out.println("Boolean value: " + boolvalue);  

     
    }

    public static void main(String[] args) {
        WrapperExample.autoBoxing();

        WrapperExample.unBoxing();
    }
}
