package com.devcamp.j02_javabasic.s50;

public class ConvertPrimitiveDataTypes {
    public static void autoBoxing(){
        byte by = 0 ;
        short sh = 0 ;
        int i = 5 ;
        long l = 7 ;
        float fl = 1.5f ;
        double db = 2.3d ;
        char ch = 'a';
        boolean boo = false ;

        // convert dữ liệu nguyên thủy sang object 
        Byte b = by ; 
        Short s = sh ;
        Integer I = i ; 
        Long L = l ;
        Float F = fl ; 
        Double D = db ; 
        Character C = ch ;
        Boolean B = boo ; 

        
        System.out.println(b);
        System.out.println(s);
        System.out.println(I);
        System.out.println(L);
        System.out.println(F);
        System.out.println(D);
        System.out.println(C);
        System.out.println(B);
    }

    public static void unBoxing(){

    // convert object sang dữ liệu nguyên thủy

        Byte b = 0 ; 
        Short s = 0 ;
        Integer I = 5 ; 
        Long L = (long) 7 ; // ép kiểu 
        Float F = 1.5f ; 
        Double D = 2.3d ; 
        Character C = 'a' ;
        Boolean B = false ; 

        byte byt = b ;
        short sho = s ;
        int in = I ;
        long lo = L ;
        float flo = F ;
        double dbo = D ;
        char cha = C;
        boolean bool = B;

        System.out.println(byt);
        System.out.println(sho);
        System.out.println(in);
        System.out.println(lo);
        System.out.println(flo);
        System.out.println(dbo);
        System.out.println(cha);
        System.out.println(bool);
          
   
    }
    public static void main(String[] args) {
        ConvertPrimitiveDataTypes.autoBoxing();
        ConvertPrimitiveDataTypes.unBoxing();    
    }
}
