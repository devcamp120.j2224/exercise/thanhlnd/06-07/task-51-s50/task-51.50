package com.devcamp.j02_javabasic.s30;

public class Customer {
    byte myByte;
    short myShortNumber;
    long myLongNumber;
    double myDoubleNumber;
    int myIntNumber;
    float myFloatNumber;
    char myLetter;
    boolean myBool;

    public Customer(){
        myByte = 127;
        myShortNumber = 2022;
        myLongNumber = 2021;
        myDoubleNumber = 123.4;
        myIntNumber = 5;
        myFloatNumber = 9.99f;
        myLetter = 'P';
        myBool = false;
    }

    public Customer(byte byteNum, short shortNum,
             long longNum, double doubleNum, int intNum, float floatNum, char letter, boolean Bool) {
        myByte = byteNum;
        myShortNumber = shortNum;
        myLongNumber = longNum;
        myDoubleNumber = doubleNum;
        myIntNumber = intNum;
        myFloatNumber = floatNum;
        myLetter = letter;
        myBool = Bool;
    }  
    
    
    
    
    public static void main(String[] args){
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 1 :");
        byte myByte = 10;
        short myShort = 2001;
        customer = new Customer(myByte,myShort,7001,671.9,231,71.99f,'A',false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 2 :");
        myByte = 20;
        myShort = 2002;
        customer = new Customer(myByte,myShort,7002,672.9,232,72.99f,'B',true);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 3 :");
        myByte = 30;
        myShort = 2003;
        customer = new Customer(myByte,myShort,7003,673.9,233,73.99f,'C',false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 4 :");
        myByte = 40;
        myShort = 2004;
        customer = new Customer(myByte,myShort,7004,674.9,234,74.99f,'D',true);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println("Run 5 :");
        myByte = 50;
        myShort = 2005;
        customer = new Customer(myByte,myShort,7005,675.9,235,75.99f,'E',false);
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
    }
}


